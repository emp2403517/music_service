package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Envirnment       string
	PostgresHost     string
	PosgresPort      int
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string
	PostgresPort     string
	LogLevel         string
	MusicServicePort  string
	MusicServiceHost  string
	DatabaseURL      string
}

func Load() Config {
	c := Config{}
	c.Envirnment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))
	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToString(getOrReturnDefault("POSTGRES_PORT", 5432))
	c.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DATABASE", "emp_musicdb"))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "postgres"))
	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "sdy12197"))
	c.DatabaseURL = cast.ToString(getOrReturnDefault("DATABASE_URL", "postgres://postgres:sdy12197@localhost:5432/emp_musicdb"))
	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.MusicServicePort = cast.ToString(getOrReturnDefault("USER_SERVICE_PORT", "5558"))
	c.MusicServiceHost = cast.ToString(getOrReturnDefault("USER_SERVICE_HOST","music"))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
