package service

import (
	"github.com/jackc/pgx/v5/pgxpool"
	mu "gitlab.com/EMP/music_service/genproto/music"
	l "gitlab.com/EMP/music_service/pkg/logger"
	grpcclient "gitlab.com/EMP/music_service/service/grpc_client"
	"gitlab.com/EMP/music_service/storage"
)

type MusicService struct {
	mu.UnimplementedMusicServiceServer
	storage storage.IStorage
	logger  l.Logger
	Client  grpcclient.GrpcClientI
}

func NewUserService(db *pgxpool.Pool, log l.Logger, client grpcclient.GrpcClientI) *MusicService {
	return &MusicService{
		storage: storage.NewStoragePg(db),
		logger:  log,
		Client:  client,
	}
}
