package service

import (
	"context"

	music "gitlab.com/EMP/music_service/genproto/music"

	l "gitlab.com/EMP/music_service/pkg/logger"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *MusicService) CreateMusic(ctx context.Context, req *music.MusicReq) (*music.MusicResp, error) {
	result, err :=s.storage.Music().CreateMusic(req)
	if err != nil {
		s.logger.Error("error while create admin", l.Any("error create admin by id", err))
		return &music.MusicResp{}, status.Error(codes.Internal, "somthing went wrong, please check id")
	}
	return result, nil
}

func (s *MusicService) GetMusicByID(ctx context.Context, req *music.ID) (*music.MusicResp, error){
	result, err :=s.storage.Music().GetMusicByID(req)
	if err != nil {
		s.logger.Error("error while inserting user", l.Any("Error insert user", err))
		return &music.MusicResp{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return result, nil
}