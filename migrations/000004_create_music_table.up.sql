CREATE TABLE musics(
    id UUID NOT NULL,
    sub_title TEXT NOT NULL,
    singer_id UUID NOT NULL,
    music_link TEXT NOT NULL,
    title VARCHAR(100),
    count INT DEFAULT 0, 
    created_at TIMESTAMP(0) WITH TIME zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP(0) WITH TIME zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    deleted_at TIMESTAMP(0) WITH TIME zone NULL
);