package main

import (
	"net"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/EMP/music_service/config"
	us "gitlab.com/EMP/music_service/genproto/music"
	//"gitlab.com/EMP/user_service/pkg/db"
	"gitlab.com/EMP/music_service/pkg/logger"
	"gitlab.com/EMP/music_service/service"
	grpcclient "gitlab.com/EMP/music_service/service/grpc_client"
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "music_service")
	defer logger.Cleanup(log)

	log.Info("main: pgxConfig",
		logger.String("host", cfg.MusicServiceHost),
		logger.Int("port", cfg.PosgresPort),
		logger.String("database", cfg.PostgresDatabase),
	)


	connDB, err := pgxpool.New(context.Background(), cfg.DatabaseURL)

	//connDB, err := pgxpool.New(cfg)

	if err != nil {
		log.Fatal("pgx connection to postgres error", logger.Error(err))
	}
	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		log.Fatal("grpc connection to client error", logger.Error(err))
	}
	userService := service.NewUserService(connDB, log, grpcClient)

	lis, err := net.Listen("tcp", ":"+cfg.MusicServicePort)

	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)

	us.RegisterMusicServiceServer(s, userService)

	log.Info("main: server running",
		logger.String("port", cfg.MusicServicePort))
	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
}