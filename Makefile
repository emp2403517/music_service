pull_submodule:
	git submodule update --init --recursive

update_submodule:	
	git submodule update --remote --merge
	
run:
	go run cmd/main.go

create_proto_submodule:
	git submodule add git@gitlab.com:dreateam/protos.git

proto-gen:
	./scripts/gen-proto.sh

swag:
	swag init -g ./api/router.go -o api/docs

migrate_music:
	migrate create -ext sql -dir migrations -seq create_users_table

migrate_category:
	migrate create -ext sql -dir migrations -seq create_addresses_table

migrate_up:
	migrate -path migrations/ -database postgres://postgres:sdy12197@localhost:5432/emp_musicdb up
	
migrate_down:
	migrate -path ./migrations/ -database postgres://postgres:sdy12197@localhost:5432/emp_musicdb down

migrate_force:
	migrate -path migrations/ -database postgres://developer:2002@localhost:5432/userdb force 1
