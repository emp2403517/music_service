package storage

import (
	"gitlab.com/EMP/music_service/storage/postgres"
	"gitlab.com/EMP/music_service/storage/repo"

	"github.com/jackc/pgx/v5/pgxpool"
)

type IStorage interface {
	Music() repo.MusicStorageI
}

type storagePg struct {
	db       *pgxpool.Pool
	musicResp repo.MusicStorageI
}

func NewStoragePg(db *pgxpool.Pool) *storagePg {
	return &storagePg{
		db:       db,
		musicResp: postgres.NewMusicRepo(db),
	}
}

func (s storagePg) Music() repo.MusicStorageI {
	return s.musicResp
}