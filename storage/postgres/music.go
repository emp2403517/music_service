package postgres

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	//"github.com/jackc/pgx/v4"
	mu "gitlab.com/EMP/music_service/genproto/music"
)

func (r *musicRepo) CreateMusic(req *mu.MusicReq) (*mu.MusicResp, error) {
	result := mu.MusicResp{}
	var created_at, updated_at time.Time
	id := uuid.New().String()
	query := `INSERT INTO musics(
		id, 
		singer_id, 
		sub_title,
		music_link,
		title)
		VALUES ($1, $2, $3, $4, $5)
		RETURNING id, singer_id, sub_title, music_link, title, count, created_at, updated_at`

	err := r.db.QueryRow(context.Background(),
		query,
		id,
		req.SingerId,
		req.SubTitle,
		req.MusicLink,
		req.Title).Scan(
		&result.Id,
		&result.SingerId,
		&result.SubTitle,
		&result.MusicLink,
		&result.Title,
		&result.Count,
		&created_at,
		&updated_at)
	if err != nil {
		fmt.Println(err)
		return &mu.MusicResp{}, err
	}

	result.CreatedAt = created_at.Format(time.RFC1123)
	result.UpdatedAt = updated_at.Format(time.RFC1123)

	return &result, nil
}
func (r *musicRepo) GetMusicByID(req *mu.ID) (*mu.MusicResp, error) {
	var created_at, updated_at time.Time
	result := mu.MusicResp{}
	query := `SELECT 
	id, 
	singer_id,
	sub_title, 
	music_link, 
	title, 
	count, 
	created_at, updated_at 
	FROM musics WHERE id=$1 AND deleted_at IS NULL`

	err := r.db.QueryRow(context.Background(), query, req.ID).Scan(
		&result.Id,
		&result.SingerId,
		&result.SubTitle,
		&result.MusicLink,
		&result.Title,
		&result.Count,
		&created_at,
		&updated_at)
	if err != nil {
		return &mu.MusicResp{}, err
	}	
	num:=result.Count
	_, err = r.db.Exec(context.Background(), `UPDATE musics SET count=$1 WHERE id=$2 AND deleted_at IS NULL`, num+1, result.Id)
	if err != nil {

	}	
	result.CreatedAt = created_at.Format(time.RFC1123)
	result.UpdatedAt = updated_at.Format(time.RFC1123)

	return &result, nil
}
