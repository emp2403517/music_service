package postgres

import (
	"github.com/jackc/pgx/v5/pgxpool"
)

type musicRepo struct {
	db *pgxpool.Pool
}

func NewMusicRepo(db *pgxpool.Pool) *musicRepo {
	return &musicRepo{db: db}
}
