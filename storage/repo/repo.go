package repo

import (
	mu "gitlab.com/EMP/music_service/genproto/music"
)

type MusicStorageI interface {
	CreateMusic(*mu.MusicReq) (*mu.MusicResp, error)
	GetMusicByID(*mu.ID) (*mu.MusicResp, error)
	
}