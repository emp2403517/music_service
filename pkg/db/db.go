package db

import (
	"context"
	"fmt"
	"os"

	"gitlab.com/EMP/music_service/config"

	"github.com/jackc/pgx/v5"
)

func ConnectToDBForSuite(cfg config.Config) (*pgx.Conn, error) {
	connDb, err := pgx.Connect(context.Background(), cfg.DatabaseURL)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	return connDb, nil
}
